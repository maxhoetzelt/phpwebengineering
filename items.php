<?php
	require "header.php";
	include "database.php";

	$query  = "SELECT card.id as id, card.name, card.picture, wants.id as wID FROM card
          LEFT JOIN wants ON card.id = wants.cardID
          where wants.id is null
          UNION
          SELECT card.id as id, card.name, card.picture, wants.id as wID FROM card
          RIGHT JOIN wants ON card.id = wants.cardID
          where wants.id is null";
	$result = $conn -> query($query);

	$i = 0;
	if (!$result)
	{
		die("Execution error");
	}
	else
	{
?>

<article>
	<table>
		<tr>
		<?php
			foreach ($result as $row)
			{
				if ($i == 5)
				{
		?>
		</tr>
		<tr>
			<td class="centered">
				<a href="item-detail.php?id=<?=$row['id']?>"><img class="card" src='pic<?=$row['picture']?>'
																  alt='<?=$row['name']?>'></a>
				<br>
				<span class="centered"><?=$row['name']?></span>
			</td>
		<?php
					$i = 1;
				}
				else
				{
		?>
			<td class="centered">
				<a href="item-detail.php?id=<?=$row['id']?>"><img class="card"
																  src='pic<?=$row['picture']?>'
																  alt='<?=$row['name']?>'></a>
				<br>
				<span class="centered"><?=$row['name']?></span>
			</td>
		<?php
					$i++;
				}
			}
		?>
		</tr>
	</table>
</article>

<?php
	}

	require "footer.html";