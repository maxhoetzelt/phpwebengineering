<?php
	session_start();

	if (isset($_SESSION["isLoggedin"]) && $_SESSION["isLoggedin"])
	{
		$href = "logout.php";
		$s    = "Logout";

		$li      = '<li><a href="my-items.php">My Items</a></li>
					<li><a href="addItem.php">Add Item</a></li>';
		$welcome = "<li id='welcome'><a href='personal.php'>Hello, " . $_SESSION["usr_name"] . "!</a></li>";
	}
	else
	{
		$href = "login.php";
		$s    = "LogIn";

		$li      = '';
		$welcome = '';
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Home</title>

		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div class="main">
			<header>
				<img src="pic/card_back.jpg" alt="logo" style="height:68px; width:46px">
				<h1>Yu-Gi-Oh! TCG Swap</h1>
			</header>
			<ul class="nav">
				<li><a href="home.php">Home</a></li>
				<li><a href="items.php">Items</a></li>
				<?=$li?>
				<li style="float: right"><a href="<?=$href?>"><?=$s?></a></li>
				<?=$welcome?>
			</ul>
