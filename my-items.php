<?php
	require "header.php";
	include "database.php";

	if (!isset($_SESSION["user_id"]))
	{
		header('Location: login.php');
	}

	$query   = "select card.id, picture, card.name, date, user.name as username
                 from card 
                 inner join owns on owns.cardID = card.id 
                 left join wants on wants.cardID = card.id 
                 left join user on wants.userID = user.id 
                 where owns.userID = " . $_SESSION["user_id"];
	$offered = $conn -> query($query);

	$query  = "select card.id, card.name, card.picture, user.name as username, user.mail
                from card 
                inner join wants on wants.cardID = card.id
                inner join owns on owns.cardID = card.id
                inner join user on user.id = owns.userID
                where wants.userID = " . $_SESSION["user_id"];
	$wanted = $conn -> query($query);

	if (!$offered && !$wanted)
	{
		die("Execution error");
	}
	else
	{
?>

<article id="myitems">
	<table class="tblmyitems">
		<tr>
			<th>Offered Cards</th>
		</tr>
		<?php
			foreach ($offered as $row)
			{
		?>
		<tr>
			<td class="centered">
				<a href="item-detail.php?id=<?=$row['id']?>">
					<img class="card" src='pic<?=$row['picture'] ?>' alt='<?=$row['name'] ?>'>
				</a>
				<br>
				<span>Offered on: <?=$row['date']?></span>
				<br>
				<span>Wanted by: <?= is_null($row['username']) ? 'none' : $row['username']?></span>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
	<table class="tblmyitems">
		<tr>
			<th>Wanted Cards</th>
		</tr>
		<?php
			foreach ($wanted as $row)
			{
		?>
		<tr>
			<td class="centered">
				<a href="item-detail.php?id=<?=$row['id']?>">
					<img class="card" src='pic<?=$row['picture'] ?>' alt='<?=$row['name'] ?>'>
				</a>
				<br>
				<span>Owner: <?=$row['username']?></span>
				<br>
				<span>Contact: <?=$row['mail']?></span>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
</article>

<?php
    }

	require "footer.html";