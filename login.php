<?php
	require "header.php";
	include "database.php";

	if (isset($_SESSION["user_id"]))
	{
		header('Location: personal.php');
	}

	$message = "";

	if (!empty($_POST["login"]))
	{
		$query = $conn -> prepare("select * from user where name = :name");
		$query -> execute(array("name"     => $_POST["name"]));
		$result = $query -> fetchALL();

		foreach($result as $row)
		{
			if (password_verify($_POST["password"], $row["password"]))
			{
				$_SESSION["user_id"]    = $row['id'];
				$_SESSION["isLoggedin"] = TRUE;
				$_SESSION["usr_name"]   = $row["name"];

				header('Location: home.php');
			}
			else
			{
				$message = "Invalid Username or Password";
			}
		}
	}

	include "login.html";
	require "footer.html";