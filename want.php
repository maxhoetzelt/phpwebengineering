<?php
	session_start();
	require "database.php";

	$query = $conn -> prepare("insert into wants(userID, cardID) values(:userID, :cardID)");
	$query -> execute(array("userID" => $_SESSION["user_id"],
							"cardID" => $_GET['id']));

	header('Location: my-items.php');