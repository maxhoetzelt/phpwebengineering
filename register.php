<?php
	require "header.php";
	include "database.php";

	if (isset($_SESSION["user_id"]))
	{
		header('Location: personal.php');
	}

	$message  = "";
	$error    = FALSE;
	$required = array('name',
					  'mail',
					  'dob',
					  'address',
					  'postcode',
					  'password',
					  'repassword');

	if (!empty($_POST["register"]))
	{
		foreach ($required as $field)
		{
			if (empty($_POST[$field]))
			{
				$error = TRUE;
			}
		}

		if ($error)
		{
			$message = "All field are required.";
		}
		elseif ($_POST["password"] != $_POST["repassword"])
		{
			$message = "Check Password.";
		}
		else
		{
			$query = $conn -> prepare("insert into user(name, mail, Address, Postcode, DateOfBirth, password)
										values(:name, :mail, :address, :postcode, :dob, :password)");
			$query -> execute(array("name"     => $_POST["name"],
									"mail"     => $_POST["mail"],
									"address"  => $_POST["address"],
									"postcode" => $_POST["postcode"],
									"dob"      => $_POST["dob"],
									"password" => password_hash($_POST["password"], PASSWORD_DEFAULT)));

			header('Location: login.php');
		}
	}

	include "register.html";
	require "footer.html";