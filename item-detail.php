<?php
	require "header.php";
	include "database.php";

	$card_id = $_GET['id'];

	$query = $conn -> prepare("select card.id, card.name, cardattr.name as attr, cardtype.name as type, level, descr, atk, def, picture, date
							   from card
							   inner join cardattr on cardattr.id = card.attr
							   inner join cardtype on cardtype.id = card.type
							   where card.id = :card_id");
	$query -> execute(array("card_id" => $card_id));
	$item = $query -> fetch();

	if (!$item)
	{
		die("Execution error");
	}
	else
	{
		$tr   = "";

		if (isset($_SESSION["isLoggedin"]) && $_SESSION["isLoggedin"])
		{
			$query = $conn -> prepare("select user.name, wants.cardID
									   from user
									   inner join owns on owns.userID = user.id
									   left join wants on owns.cardID = wants.cardID
									   where owns.cardID = :card_id");
			$query -> execute(array("card_id" => $card_id));
			$card = $query -> fetch();

			if (!$card)
			{
				die("Execution Error");
			}
			else
			{
				if (is_null($card["cardID"]))
				{
                    $tr = "<tr>
                            <td>
                            	<form id='frmWant' action='want.php'>
                            		<button class='form-submit-button' type='submit' name='id' value='" . $card_id. "'>Want</button>
								</form>
							</td>
                            <th>Owner</th>
                            <td>" . $card["name"] . "</td>
                           </tr>";
                }
                else
                {
                    $tr = "<tr>
                            <td class='centered'>wanted</td>
                            <th>Owner</th>
                            <td>" . $card["name"] . "</td>
                           </tr>";
                }
            }
        }
?>

<article>
	<table>
		<tr>
			<td rowspan="8"><img class="card" src='pic<?=$item['picture'] ?>' alt='<?=$item['name'] ?>'></td>
			<th>Name</th>
			<td><?=$item['name']?></td>
		</tr>
		<tr>
			<th>Attribute</th>
			<td><?=$item['attr']?></td>
		</tr>
		<tr>
			<th>Type</th>
			<td><?=$item['type']?></td>
		</tr>
		<tr>
			<th>Level</th>
			<td><?=$item['level']?></td>
		</tr>
		<tr>
			<th>Description</th>
			<td><?=$item['descr']?></td>
		</tr>
		<tr>
			<th>Attack</th>
			<td><?=$item['atk']?></td>
		</tr>
		<tr>
			<th>Defense</th>
			<td><?=$item['def']?></td>
		</tr>
		<tr>
			<th>Date of Post</th>
			<td><?=$item['date']?></td>
		</tr>
		<?=$tr?>
	</table>
</article>

<?php
	}

	require "footer.html";