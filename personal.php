<?php
	require "header.php";
	include "database.php";

	if (!isset($_SESSION["user_id"]))
	{
		header('Location: login.php');
	}

	$query = $conn -> prepare("select * from user where id = :id");
	$query -> execute(array("id" => $_SESSION["user_id"]));
	$result = $query -> fetch();

?>

	<article>
		<table>
			<tr>
				<th>Name</th>
				<td><?=$result['name']?></td>
			</tr>
			<tr>
				<th>Contact</th>
				<td><?=$result['mail']?></td>
			</tr>
			<tr>
				<th>Address</th>
				<td><?=$result['Address']?></td>
			</tr>
			<tr>
				<th>ZIP</th>
				<td><?=$result['Postcode']?></td>
			</tr>
			<tr>
				<th>Date of Birth</th>
				<td><?=$result['DateOfBirth']?></td>
			</tr>
		</table>
	</article>

<?php
	require "footer.html";