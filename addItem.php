<?php
	require "header.php";
	include "database.php";

	if (!isset($_SESSION["user_id"]))
	{
		header('Location: login.php');
	}

	$message  = "";
	$error    = FALSE;
	$required = array('name',
					  'desc',
					  'attr',
					  'type',
					  'lvl',
					  'atk',
					  'def');
	$date     = date("Y-m-d", time());

	$target_dir = "pic/cards/";

	$uploadOk = 1;

	$query = $conn -> prepare("select * from cardattr");
	$query -> execute();
	$attr = $query -> fetchAll();

	$query = $conn -> prepare("select * from cardtype");
	$query -> execute();
	$type = $query -> fetchAll();

	if (!empty($_POST["addItem"]) && isset($_FILES['pic']))
	{
		foreach ($required as $field)
		{
			if (empty($_POST[$field]))
			{
				$error = TRUE;
			}
		}
		if (empty($_FILES["pic"]["name"]))
		{
			$error = TRUE;
		}

		if ($error)
		{
			$message = "All field are required.";
		}
		else
		{
			$target_file   = $target_dir . basename($_FILES["pic"]["name"]);
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

			$check = getimagesize($_FILES["pic"]["tmp_name"]);
			if ($check !== FALSE)
			{
				$uploadOk = 1;
			}
			else
			{
				$message  = "File is not an image!";
				$uploadOk = 0;
			}

			if ($uploadOk == 0)
			{

			}
			else
			{
				if (move_uploaded_file($_FILES["pic"]["tmp_name"], $target_file))
				{
					$query = $conn -> prepare("insert into card(name, attr, type, level, descr, atk, def, date, picture)
										values(:name, :attr, :type, :level, :descr, :atk, :def, :date, :picture)");
					$query -> execute(array("name"    => $_POST["name"],
											"attr"    => $_POST["attr"],
											"type"    => $_POST["type"],
											"level"   => $_POST["lvl"],
											"descr"   => $_POST["desc"],
											"atk"     => $_POST["atk"],
											"def"     => $_POST["def"],
											"date"    => $date,
											"picture" => "/cards/" . basename($_FILES["pic"]["name"])));

					$query = $conn -> prepare("select id from card where name = :name");
					$query -> execute(array("name" => $_POST["name"]));
					$id = $query -> fetch();

					$query = $conn -> prepare("insert into owns(userID, cardID) values(:userID, :cardID)");
					$query -> execute(array("userID" => $_SESSION["user_id"],
											"cardID" => $id["id"]));

					header('Location: item-detail.php?id=' . $id["id"]);
				}
				else
				{
					$message = "Sorry, there was an error uploading your file.";
				}
			}

		}
	}

	(include "addItem.html") or die("Sorry, the page went missing");

	require "footer.html";